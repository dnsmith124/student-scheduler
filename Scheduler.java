package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Scheduler {
	public static void main(String[] args) {
		ArrayList<Student> studentList = new ArrayList<Student>();

		ArrayList<String> daysAvailable = new ArrayList<String>();
		daysAvailable.add("Sunday");
		daysAvailable.add("Monday");
		daysAvailable.add("Tuesday");
		daysAvailable.add("Wednesday");
		daysAvailable.add("Thursday");
		daysAvailable.add("Friday");
		//daysAvailable.add("Saturday");

		File studentFile = new File("C:\\Users\\zmwal\\eclipse-workspace\\Student Scheduler\\src\\core\\students.txt");
		String line;
		try (BufferedReader fileInput = new BufferedReader(new FileReader(studentFile))) {
			while ((line = fileInput.readLine()) != null) {
				String name = line;
				String day = fileInput.readLine();
				int time = Integer.parseInt(fileInput.readLine());
				Student newStudent = new Student(name, day, time);
				studentList.add(newStudent);
				showInfo(studentList);
				detectConflicts(studentList, daysAvailable);
				confirmAvailableDays(studentList, daysAvailable);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file " + studentFile.toString());
		} catch (IOException e1) {
			System.out.println("Can't read file " + studentFile.toString());
		}
	}

	public static void showInfo(ArrayList<Student> list) {
		System.out.println("Student List: ");
		System.out.println();
		for (Student s : list) {
			System.out.println("ID: " + s.getID());
			System.out.println("Name: " + s.getName());
			System.out.println("Day: " + s.getLessonDay());
			System.out.println("Time: " + s.getLessonTime());
			System.out.println();
		}
	}

	public static void detectConflicts(ArrayList<Student> list, ArrayList<String> dayList) {
		boolean conflict = false;
		for (int i = 0; i < list.size(); i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(i).getLessonDay().equals(list.get(j).getLessonDay())
						&& list.get(i).getLessonTime() == (list.get(j).getLessonTime())) {
					System.out.println("A conflict exists between " + list.get(i).getName() + " and "
							+ list.get(j).getName() + " at " + list.get(i).getLessonTime() + " on " + list.get(i).getLessonDay() + ".");
					conflict = true;
				}
			}
		}
		if (conflict == false) {
			System.out.println("No schedule conflicts detected.");
		}
	}

	public static void confirmAvailableDays(ArrayList<Student> list, ArrayList<String> dayList) {
		boolean conflict = false;
		for (int k = 0; k < list.size(); k++) {
			if (dayList.contains(list.get(k).getLessonDay()) != true) {
				System.out
						.println(list.get(k).getName() + " has a lesson on a day you don't teach. Please reschedule.");
				conflict = true;
			}
		}
		if (conflict == false) {
			System.out.println("No students scheduled on non-teaching days.");
		}
	}

	public static void displayOpenings(ArrayList<Student> list, ArrayList<String> dayList) {

	}

	public static void showLessonDays(ArrayList<Student> list) {
		System.out.printf("Days with students: ");
		for (Student s : list) {
			System.out.printf(s.getLessonDay() + " ");
		}
	}

	public static void showStudentNames(ArrayList<Student> list) {
		System.out.printf("Students in list: ");
		for (Student s : list) {
			System.out.print(s.getName() + " ");
		}
	}

	public static void showLessonTimes(ArrayList<Student> list) {
		System.out.printf("Times in list: ");
		for (Student s : list) {
			System.out.print(s.getLessonTime() + " ");
		}
	}
	
	public static void addStudent(ArrayList<Student> list, Student student) {
		list.add(student);
	}

	public static void removeStudent(ArrayList<Student> list, Student student) {
		list.remove(student);
	}
}
