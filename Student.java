package core;

public class Student {
	private String name;
	private String lessonDay;
	private int lessonTime;
	private int ID;
	private static int numberOfStudents;

	public Student() {
		this("name_not_found", "day_not_found", -1);
		ID = ++numberOfStudents;
	}

	public Student(String name) {
		this(name, "day_not_found", -1);
		ID = ++numberOfStudents;
	}

	public Student(String name, String lessonDay) {
		this(name, lessonDay, -1);
		ID = ++numberOfStudents;
	}

	public Student(String name, String lessonDay, int lessonTime) {
		this.name = name;
		this.lessonDay = lessonDay;
		this.lessonTime = lessonTime;
		ID = ++numberOfStudents;
	}
	
	public void setID(int iD) {
		ID = iD;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLessonDay(String lessonDay) {
		this.lessonDay = lessonDay;
	}

	public void setLessonTime(int lessonTime) {
		this.lessonTime = lessonTime;
	}
	
	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public String getLessonDay() {
		return lessonDay;
	}

	public int getLessonTime() {
		return lessonTime;
	}

	public void getInfo() {
		System.out.println("Student " + name + " has a lesson on " + lessonDay + " at " + lessonTime + ".");
	}

	public static void conflict(Student student1, Student student2) {
		if (student1.lessonDay == student2.lessonDay && student1.lessonTime == student2.lessonTime) {
			System.out.println(student1.name + " and " + student2.name + " have a conflict!");
		} else {
			System.out.println(student1.name + " and " + student2.name + " have no conflict.");
		}
	}
}
